import React, { Component } from 'react';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom'
import Home from './components/Home'
import AccountBalance from './components/AccountBalance';
import UserProfile from './components/UserProfile'
class App extends Component {

constructor(){
super();
    this.state = {
      accountBalance: 500,
      UserName: "Kenn",
      Company: "Block"
    }
}
render() 
  {
    const HomeComponent = ()=>(<Home  accountBalance={this.state.accountBalance}/>)
    const UserProfileComponent = () => (<UserProfile
      username = {this.state.UserName}
      company = {this.state.Company} />)
    return (
      <Router>
        <Switch>
          <Route exact path="/" render={HomeComponent}/>
          <Route exact path="/user" render={UserProfileComponent}/>
        </Switch>
      </Router>
    );
  }
}

export default App;
